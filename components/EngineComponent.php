<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\helpers\Html;
use codemix\yii2confload\Config;
use kartik\popover\PopoverX;
use yii\helpers\Url;

/*
 *  clase que sirve como repositorio de metodos para operaciones disponibles globalmente en toda la app 
 *  permite acceder a toda la app desde una opcion publica o privada
 */

class EngineComponent extends Component {
    /*
     * return id del usuario firmado en el sistema
     */

    public function fechaSql($fecha) {
        if ($fecha != "") {
            $f = explode('/', $fecha);
            if (count($f) == 3) {
                $mes = strlen($f[1]) == 1 ? '0' . $f[1] : $f[1];
                $fecha = $f[2] . '-' . $mes . '-' . $f[0];
            }
        }
        return $fecha;
    }

    /*
     * valida el campo rut 
     */

    public function validateRut($rut) {
        $res = true;
        if ($rut != "") {
            $data = explode('-', $rut);
            if (strlen($rut) < 9 || strlen($rut) > 10 || count($data) != 2 || strlen($data[1]) != 1) {
                $res = false;
            }
            $data = explode('-', $rut);
            $evaluate = strrev($data[0]);
            $multiply = 2;
            $store = 0;
            for ($i = 0; $i < strlen($evaluate); $i++) {
                $store += $evaluate[$i] * $multiply;
                $multiply++;
                if ($multiply > 7)
                    $multiply = 2;
            }
            isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
            $result = 11 - ($store % 11);
            if ($result == 10)
                $result = 'k';
            if ($result == 11)
                $result = 0;
            if ($verifyCode != $result) {
                $res = false;
            }
        }
        return $res;
    }

    

    //************************ kilometros recorridos
    // https://rest.bermanngps.cl:40001/BermannRest/api/ulpos?tk=d48a79a37e3d65b45a57587bc0481902&patente=Camioneta%20Ford%20Arturo

    public function getToken() {
        $token = 0;
        $usuario = 'bermannsa';
        $clave = '3b24fa7e14555a1912e5bc2198f8e075';
        $url = $this->api_rest1 . 'login?user=' . $usuario . '&pass=' . $clave . '&app=1&os=1';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $resultArray = json_decode($result, true);
        if ($resultArray['estado'] == "OK") {
            if ($resultArray['mensaje'][0]['estado'] == "OK") {
                $token = $resultArray['mensaje'][0]['token'];
            }
        }
        return $token;
    }

    public function getTokenComandos($usuario, $claveStr) {
        $token = 0;
        $clave = md5($claveStr);
        $urlBase = 'https://rest2.bermanngps.cl/BermannRest/api/login';
        $url = $urlBase . '?user=' . $usuario . '&pass=' . $clave . '&app=9&os=3';
        // echo $url;
        $ch = curl_init();
        ini_set("curl.cainfo", null);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $resultArray = json_decode($result, true);
        if ($resultArray['estado'] == "OK") {
            if ($resultArray['mensaje'][0]['estado'] == "OK") {
                $token = $resultArray['mensaje'][0]['token'];
            }
        }
        return $token;
    }

    public function getCurrentUserSkyName(){
        $session = Yii::$app->session;
        $idUsuario = $session['IdUsuario'];
        $usuarios = \app\models\Usuarios::find()->where(['id'=>$idUsuario])->one();
        $res = '';
        if ($usuarios!=false){
            $res = $usuarios->usuario_sky;
        }
        return $res;
    }
    
    public function getCurrentUserSkyPass(){
        $session = Yii::$app->session;
        $idUsuario = $session['IdUsuario'];
        $usuarios = \app\models\Usuarios::find()->where(['id'=>$idUsuario])->one();
        $res = '';
        if ($usuarios!=false){
            $res = $usuarios->clave_sky;
        }
        return $res;
    }

    
    
    // ****************************************************** VISTA SKYVIEW ********************
    
    
    
    
    
    
    
    
    
    
    
    
    // ******************************************************
    
    

}
