<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAaNYrz3KAgkFycB0Fbs4b_GYtfVWNIXVU&libraries=geometry,places&token=48e085e4c501958a3b17f1df59c28c4d"></script>
<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\View;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Collapse;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

$this->title = 'SkyView Vista - Bermann';
$this->params['bread1'] = $this->title;
$this->params['activeLink'] = "inicio";

ini_set('max_execution_time', '99600');
ini_set('max_input_time', '-1');
ini_set('memory_limit', '-1');
set_time_limit(9600);

$imagenFlechaVerde = '<img src="' . Yii::getAlias('@web') . '/images/flecha_verde.png" style="width:16px;height:16px;" />';
$imagenFlechaAmarilla = '<img src="' . Yii::getAlias('@web') . '/images/flecha_amarilla.png" style="width:16px;height:16px;" />';
$imagenFlechaRoja = '<img src="' . Yii::getAlias('@web') . '/images/flecha_roja.png" style="width:16px;height:16px;" />';
$imagenFlechaStop = '<img src="' . Yii::getAlias('@web') . '/images/stop_1.png" style="width:16px;height:16px;" />';
$imagenFlechaStop2 = '<img src="' . Yii::getAlias('@web') . '/images/stop_2.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';
$imagenFlechaStop3 = '<img src="' . Yii::getAlias('@web') . '/images/stop_3.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';
?>

<?php
  if (isset($_GET['token'])==true && $_GET['token']!=""){
      Yii::$app->session->set('token_apisky',$_GET['token']);
  }
?>
<div class="frow">
    <div class="col col3">
        <div style="" class="listado_class" id="header_listado">
            <div style="text-align:left;padding:3px;">
                <span><b>Vehículos</b>&nbsp;</span><span id="cantidad_vehiculos" class="badge badge-danger"></span><br/>
                <button class="btn btn-primary" onclick="centrarVistaArrMarkersTodos();">Centrar Mapa</button>
                &nbsp;&nbsp;
                <button class="btn btn-primary" onclick="getVehiculosGrupos();">Refrescar Datos</button>
            </div>
            <div style="display: inherit">
                <label class="control-label">Mostrar Patente o Muestra</label>
                <div  style="margin-bottom:8px;">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-success active" style="width:150px;" onclick="setMostrar(1);">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Patente
                        </label>
                        <label class="btn btn-success" style="width:150px;" onclick="setMostrar(2);">
                            <input type="radio" name="options" id="option2" autocomplete="off"> Muestra
                        </label>
                    </div>
                </div>
                <div class="form-group" id="patente_div">
                    <?php
                    echo Select2::widget([
                        'name' => 'patente',
                        'data' => [],
                        'options' => [
                            'placeholder' => 'Seleccione Patente..',
                            'multiple' => false,
                            'id' => 'patente',
                            'class' => 'form-control',
                            'style' => 'border:1em solid red;font-size:12px',
                        ],
                        'pluginEvents' => [
                            "change" => "function() { buscaPatente(this.value); }",
                        ],
                        'pluginOptions' => [
                            'dropdownCssClass' => 'listado_patentes',
                            'allowClear' => false,
                            'theme' => 'classic',
                        ],
                    ]);
                    ?>  
                </div>
            </div>
            <div style="margin-bottom:5px;">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-warning btn-sm" style="width:150px;" onclick="$('#listado_grupos .panel-collapse').collapse('show');">
                        <input type="radio" name="options" id="option1" autocomplete="off" checked> Expandir
                    </label>
                    <label class="btn btn-warning btn-sm active" style="width:150px;" onclick="$('#listado_grupos .panel-collapse').collapse('hide');">
                        <input type="radio" name="options" id="option2" autocomplete="off"> Contraer
                    </label>
                </div>
            </div>
            <div id="listados" style="vertical-align:top;width:auto;min-height:350px;height: calc(100vh - 100px);max-height: 400px;overflow-x:hidden;overflow-y: scroll"></div>
        </div>
    </div>
    <div class="col col4" style=""> 
        <div id="mensajes"></div>
        <div id="playbackdiv" style="display:none">
            <div style="padding:5px;">
                <h5 class="modal-title">Playback - <span id="tituloplayback"></span></h5>
                <div class="alert alert-info" style="padding:5px;color:black">
                    Seleccione rango de fecha y hora para consultar trayectoria registrada a un vehiculo. Por defecto se muestra un rango desde las últimas 4 horas en el dia de hoy.
                </div>
                <table border="0" cellspacing="2" cellpadding="2" class="table-responsive dataTable" style="width:100%;margin-bottom: 10px;font-size: 14px;">
                    <tbody>
                        <tr>
                            <td style="padding:2px;vertical-align:top;width:10%;max-width:10%">
                                <div class="form-group">
                                    <?php
                                    echo '<label class="control-label">Fecha Inicio</label>';
                                    echo DatePicker::widget([
                                        'name' => 'fecha_desde',
                                        'removeButton' => false,
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => date('d/m/Y'),
                                        'options' => [
                                            'placeholder' => 'fecha inicio ...',
                                            'class' => 'form-control datetimepicker datetimepicker-input',
                                            'style' => ['font-size:14px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                                            'id' => 'fecha_desde',
                                        ],
                                        'pluginEvents' => [
                                            'changeDate' => 'function(e) {  validaFechas(); }',
                                        ],
                                        'pluginOptions' => [
                                            'endDate' => 'new date()',
                                            'convertFormat' => true,
                                            'orientation' => 'bottom left',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'autoclose' => true,
                                        ]
                                    ]);
                                    ?>
                                </div> 
                            </td>
                            <td style="padding:2px;vertical-align:top;width:10%;max-width:10%">
                                <div class="form-group">
                                    <?php
                                    echo '<label class="control-label">Hora Inicio</label>';
                                    echo TimePicker::widget([
                                        'name' => 'hora_desde',
                                        'size' => 'md',
                                        'addonOptions' => [
                                            'asButton' => false,
                                            'buttonOptions' => ['style' => 'display:none']
                                        ],
                                        'options' => [
                                            'placeholder' => 'hora inicio ...',
                                            'class' => 'form-control',
                                            'style' => ['font-size:12px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                                            'id' => 'hora_desde',
                                            'maxlength' => 5,
                                        ],
                                        'pluginOptions' => [
                                            'showRightIcon' => false,
                                            'showSeconds' => false,
                                            'showMeridian' => false,
                                            'minuteStep' => 1,
                                            'defaultTime' => date('H:i', strtotime('-4 hour', time())),
                                            'template' => false
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </td>
                            <td style="padding:2px;vertical-align:top;width:10%;max-width:10%">
                                <div class="form-group">
                                    <?php
                                    echo '<label class="control-label">Fecha Fin</label>';
                                    echo DatePicker::widget([
                                        'name' => 'fecha_hasta',
                                        'removeButton' => false,
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => date('d/m/Y'),
                                        'options' => [
                                            'placeholder' => 'fecha fin ...',
                                            'class' => 'form-control datetimepicker datetimepicker-input',
                                            'style' => ['font-size:14px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                                            'id' => 'fecha_hasta',
                                        ],
                                        'pluginEvents' => [
                                            'changeDate' => 'function(e) {  validaFechas(); }',
                                        ],
                                        'pluginOptions' => [
                                            'endDate' => 'new date()',
                                            'convertFormat' => true,
                                            'orientation' => 'bottom left',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'autoclose' => true,
                                        ]
                                    ]);
                                    ?> 
                                </div>
                            </td>        
                            <td style="padding:2px;vertical-align:top;width:10%;max-width:10%">
                                <div class="form-group">
                                    <?php
                                    echo '<label class="control-label">Hora Fin</label>';
                                    echo TimePicker::widget([
                                        'name' => 'hora_hasta',
                                        'size' => 'md',
                                        'addonOptions' => [
                                            'asButton' => false,
                                            'buttonOptions' => ['style' => 'display:none']
                                        ],
                                        'options' => [
                                            'placeholder' => 'hora fin ...',
                                            'class' => 'form-control',
                                            'style' => ['font-size:14px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                                            'id' => 'hora_hasta',
                                            'maxlength' => 5,
                                        ],
                                        'pluginOptions' => [
                                            'showRightIcon' => false,
                                            'showSeconds' => false,
                                            'showMeridian' => false,
                                            'minuteStep' => 1,
                                            'defaultTime' => date('H:i'),
                                            'template' => false
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </td>
                            <td style="padding:2px;vertical-align:top;width:10%;max-width:10%">
                                <button type="button" class="btn btn-primary" onclick="buscaPlayback();">Buscar datos</button> 
                            </td>
                            <td style="padding:2px;vertical-align:top;width:10%;max-width:10%">
                                <button style="width:130px;" type="button" class="btn btn-danger" onclick="$('#playbackdiv').hide('slow');">Cerrar Playback</button><br/>
                                <button style="width:130px;" type="button" class="btn btn-warning" onclick="apagaPlayback();">Terminar Playback</button><br/>
                                <button style="width:130px;" class="btn btn-primary" onclick="centrarVistaArrMarkers();">Centrar Mapa Playback</button>
                            </td>
                        </tr>
                    </tbody>
                </table>              
            </div>
        </div>
        <div id="map" class="gmaps"></div>
    </div>  
</div>

<script>

    var mostrar = 1;
    var actualizo = 0;

    /*var flecha_verde = '<?= $imagenFlechaVerde; ?>';
     var flecha_amarilla = '<?= $imagenFlechaAmarilla; ?>';
     var flecha_roja = '<?= $imagenFlechaRoja; ?>';
     var flecha_stop = '<?= $imagenFlechaStop; ?>';
     var flecha_stop2 = '<?= $imagenFlechaStop2; ?>';
     var flecha_stop3 = '<?= $imagenFlechaStop3; ?>';
     */

    var listadoMarkers = [];


    $(document).ready(function () {
        initialize();
        setInterval(function () {
            if (actualizo == 1) {
                getVehiculosGrupos();
            }
        }, 60000);

    });

    var map = null;
    var markersArray = [];
    var vehiculosArray = [];
    var geocoder = null;

    function calculoTiempoDetenidoMinutos(tiempo_detenido) {
        var res = 0;
        var timeArray = tiempo_detenido.split(":");
        if (timeArray.length > 0) {
            res = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
        }
        return res;
    }

    function getFlechaMovil(vehiculo) {
        // logica iconos y colores 
        var res = '';
        var color = 0;
        var rotacion = parseInt(vehiculo.orientacion);
        var velocidad = parseInt(vehiculo.velocidad);
        var tiempo_detenido_minutos = calculoTiempoDetenidoMinutos(vehiculo.tiempo_detenido);
        // verde
        if (tiempo_detenido_minutos > 0 && tiempo_detenido_minutos <= 5 && velocidad < 5) {
            color = 1;
        }
        // roja
        if (tiempo_detenido_minutos > 5 && tiempo_detenido_minutos <= 25 && velocidad < 5) {
            color = 2;
        }
        // stop rojo
        if (tiempo_detenido_minutos > 25 && tiempo_detenido_minutos <= 2900 && velocidad < 5) {
            color = 4;
        }
        // stop negro 
        if (tiempo_detenido_minutos > 2900 && velocidad < 5) {
            color = 5;
        }

        // stop
        if (color == 4) {
            res = flecha_stop;
        }
        if (color == 5) {
            res = flecha_stop2;
        }

        if (color < 4) {
            if (color == 0) {
                res = flecha_verde;
            }
            if (color == 1) {
                res = flecha_amarilla;
            }
            if (color == 2) {
                res = flecha_roja;
            }
        }
        return res;
    }

    function getIconoMovil(vehiculo) {
        // logica iconos y colores 
        var color = 0;
        var rotacion = parseInt(vehiculo.orientacion);
        var velocidad = parseInt(vehiculo.velocidad);
        var tiempo_detenido_minutos = calculoTiempoDetenidoMinutos(vehiculo.tiempo_detenido);
        // verde
        if (tiempo_detenido_minutos > 0 && tiempo_detenido_minutos <= 5 && velocidad < 5) {
            color = 1;
        }
        // roja
        if (tiempo_detenido_minutos > 5 && tiempo_detenido_minutos <= 25 && velocidad < 5) {
            color = 2;
        }
        // stop rojo
        if (tiempo_detenido_minutos > 25 && tiempo_detenido_minutos <= 2900 && velocidad < 5) {
            color = 4;
        }
        // stop negro 
        if (tiempo_detenido_minutos > 2900 && velocidad < 5) {
            color = 5;
        }
        var fillColor = ["green", "yellow", "red", "white"];
        var shape = {
            gray: 'm 15.832471,-142.36175 c 0,0 121.076359,-2.15352 114.751919,317.15236 -83.675559,-59.30589 -133.675559,-59.30589 -229.503849,0 -4.171711,-319.30588 115.82829,-319.30588 114.75193,-317.15236 z',
            blue: 'm -35.860415,-201.21297 c 0,0 76.278938,198.1153079 110.756059,306.10857 -95.39889,-79.999013 -125.957928,-58.687449 -221.512124,0 z'
        };
        var url = "<?= Yii::getAlias('@web'); ?>/images/stop_1.png";
        var url2 = "<?= Yii::getAlias('@web'); ?>/images/stop_3.png";

        // stop
        if (color == 4) {
            var image = {
                url: url,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32)
            };
        }
        if (color == 5) {
            var image = {
                url: url2,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32)
            };
        }

        if (color < 4) {
            var image = {
                path: shape["blue"], fillColor: fillColor[color],
                fillOpacity: 1,
                scale: 0.1, strokeColor: "black",
                strokeWeight: 1,
                rotation: parseInt(rotacion)};
        }
        return image;
    }

    function getIconoMovilPlayback(vehiculo) {
        // logica iconos y colores 
        var color = vehiculo.color;
        var rotacion = parseInt(vehiculo.orientacion);
        var velocidad = parseInt(vehiculo.velocidad);

        var fillColor = ["green", "yellow", "red", "white"];
        var shape = {
            gray: 'm 15.832471,-142.36175 c 0,0 121.076359,-2.15352 114.751919,317.15236 -83.675559,-59.30589 -133.675559,-59.30589 -229.503849,0 -4.171711,-319.30588 115.82829,-319.30588 114.75193,-317.15236 z',
            blue: 'm -35.860415,-201.21297 c 0,0 76.278938,198.1153079 110.756059,306.10857 -95.39889,-79.999013 -125.957928,-58.687449 -221.512124,0 z'
        };
        var url = "<?= Yii::getAlias('@web'); ?>/images/stop_1.png";
        var url2 = "<?= Yii::getAlias('@web'); ?>/images/stop_3.png";

        // stop
        if (color == 4) {
            var image = {
                url: url,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32)
            };
        }
        if (color == 5) {
            var image = {
                url: url2,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32)
            };
        }

        if (color < 4) {
            var image = {
                path: shape["blue"], fillColor: fillColor[color],
                fillOpacity: 1,
                scale: 0.1, strokeColor: "black",
                strokeWeight: 1,
                rotation: parseInt(rotacion)};
        }
        return image;
    }

    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(-33.446420, -70.660569),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 12
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        geocoder = new google.maps.Geocoder();
        getVehiculosGrupos();
    }

    function getDireccion(vehiculoData, infow, mark, event) {
        var v = vehiculoData;
        if (v != null && v != undefined) {
            var lat = vehiculoData.latitud;
            var lng = vehiculoData.longitud;
            if (lat == undefined || lat == '') {
                return false;
            }
            if (lng == undefined || lng == '') {
                return false;
            }

            var datos = document.createElement('div');
            datos.style.width = '500px';
            datos.style.height = '250px';
            var close = document.createElement('div');
            close.align = "center";
            var closeLink = document.createElement('a');
            closeLink.innerHTML = "<span class='btn btn-danger btn-mini'><b>Cerrar</b></span>";
            closeLink.href = "#";
            closeLink.onclick = function () {
                infow.close();
            };
            var tiempo = vehiculoData.tiempo_detenido;
            close.appendChild(closeLink);
            var contentString = document.createElement('div');
            contentString.align = "left";
            contentString.innerHTML = '<div style="padding:5px;width:auto;">' +
                    '<i class="fas fa-truck-moving fa-2x text-success"></i> <b><span style="margin-left:5px;font-size:1.5em">Detalle Vehículo</span></b>' +
                    '<br/><br/>' +
                    '<table border="0" cellspacing="2" cellpadding="2"><tbody>' +
                    '<tr><td style="text-align:left;padding:4px;width:120px;"><b>Fecha : </b></td><td>&nbsp;&nbsp;</td><td>' + formatDateSky(vehiculoData.fecha) + '</td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Patente : </b></td><td>&nbsp;&nbsp;</td><td>' + vehiculoData.patente + '</td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Muestra : </b></td><td>&nbsp;&nbsp;</td><td>' + vehiculoData.muestra + '</td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Direccion : </b></td><td>&nbsp;&nbsp;</td><td>' + vehiculoData.direccion + '</td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Velocidad : </b></td><td>&nbsp;&nbsp;</td><td>' + vehiculoData.velocidad + ' </td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Tiempo Detenido : </b></td><td>&nbsp;&nbsp;</td><td>' + tiempo + '</td></tr>' +
                    '</tbody></table>' +
                    '</div>';
            datos.appendChild(contentString);
            datos.appendChild(close);
            infow.setContent(datos);
            if (event != undefined) {
                infow.setPosition(event.LatLng);
            }
            infow.open(map, mark);
            return true;
        } else {
            return false;
        }
    }

    function formatDateSky(datestr) {
        if (datestr == "") {
            return false;
        }
        var skydate = new Date(datestr);
        return skydate.getDate() + "/" + skydate.getMonth() + "/" + skydate.getFullYear() + " " + skydate.getHours() + ":" + skydate.getMinutes();
    }

    function getDireccionPlayback(vehiculoData, infow, mark, event) {
        var v = vehiculoData;
        if (v != null && v != undefined) {
            var lat = vehiculoData.latitud;
            var lng = vehiculoData.longitud;
            if (lat == undefined || lat == '') {
                return false;
            }
            if (lng == undefined || lng == '') {
                return false;
            }

            var datos = document.createElement('div');
            datos.style.width = '500px';
            datos.style.height = '200px';
            var close = document.createElement('div');
            close.align = "center";
            var closeLink = document.createElement('a');
            closeLink.innerHTML = "<span class='btn btn-danger btn-mini'><b>Cerrar</b></span>";
            closeLink.href = "#";
            closeLink.onclick = function () {
                infow.close();
            };
            close.appendChild(closeLink);
            var contentString = document.createElement('div');
            contentString.align = "left";
            contentString.innerHTML = '<div style="padding:5px;width:auto;">' +
                    '<i class="fas fa-truck-moving fa-2x text-success"></i> <b><span style="margin-left:5px;font-size:1.5em">Detalle Vehículo</span></b>' +
                    '<br/><br/>' +
                    '<table border="0" cellspacing="2" cellpadding="2"><tbody>' +
                    '<tr><td style="text-align:left;padding:4px;width:120px;"><b>Fecha : </b></td><td>&nbsp;&nbsp;</td><td>' + formatDateSky(vehiculoData.evfecha) + '</td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Direccion : </b></td><td>&nbsp;&nbsp;</td><td>' + vehiculoData.direccion + '</td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Velocidad : </b></td><td>&nbsp;&nbsp;</td><td>' + vehiculoData.velocidad + ' </td></tr>' +
                    '<tr><td style="text-align:left;padding:4px;"><b>Distancia : </b></td><td>&nbsp;&nbsp;</td><td>' + vehiculoData.distancia + ' </td></tr>' +
                    '</tbody></table>' +
                    '</div>';
            datos.appendChild(contentString);
            datos.appendChild(close);
            infow.setContent(datos);
            if (event != undefined) {
                infow.setPosition(event.LatLng);
            }
            infow.open(map, mark);
            return true;
        } else {
            return false;
        }
    }

    google.maps.Polyline.prototype.getBounds = function () {
        var bounds = new google.maps.LatLngBounds();
        this.getPath().forEach(function (e) {
            bounds.extend(e);
        });
        return bounds;
    };

    function centrarVistaArrMarkers()
    {
        var ArrMarkers = playbackMarkersLista;
        if (ArrMarkers.length > 0)
        {
            fakeLine = new google.maps.Polyline({
                path: ArrMarkers,
                strokeColor: '#FF0000',
                strokeOpacity: 0.5,
                strokeWeight: 3
            });
            fakeLine.setMap(map);
            map.fitBounds(fakeLine.getBounds());
            fakeLine.setMap(null);
        } else {
            var defaultLat = -33.449167;
            var defaultLon = -70.646959;
            var var_location = new google.maps.LatLng(defaultLat, defaultLon);
            map.panTo(var_location);
            map.setZoom(9);
        }
    }

    function centrarVistaArrMarkersTodos()
    {
        var ArrMarkers = playbackMarkersLista2;
        if (ArrMarkers.length > 0)
        {
            fakeLine = new google.maps.Polyline({
                path: ArrMarkers,
                strokeColor: '#FF0000',
                strokeOpacity: 0.5,
                strokeWeight: 3
            });
            fakeLine.setMap(map);
            map.fitBounds(fakeLine.getBounds());
            fakeLine.setMap(null);
        } else {
            var defaultLat = -33.449167;
            var defaultLon = -70.646959;
            var var_location = new google.maps.LatLng(defaultLat, defaultLon);
            map.panTo(var_location);
            map.setZoom(9);
        }
    }


    // *************************** VISTA SKYVIEW ***************

    /* REST-2  */
    function setMostrar(valor) {
        if (mostrar == valor) {
            return false;
        }
        mostrar = valor;
        $(".patentepatente").hide();
        $(".patentemuestra").hide();
        if (mostrar == 1) {
            $(".patentepatente").show();
        }
        if (mostrar == 2) {
            $(".patentemuestra").show();
        }
    }

    function formatDateRest2(datestr) {
        var date = new Date(datestr);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var hour = date.getHours();
        var minutes = date.getMinutes();
        var day = date.getDate();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        if (hour < 10) {
            hour = '0' + hour;
        }
        if (minutes < 10) {
            minutes = '0' + minutes;
        }
        return day + '/' + month + ' ' + hour + ':' + minutes;
    }

    var gruposVehiculosArrayRest2 = [];
    var vehiculosArrayRest2 = [];

    var flecha_verde = '<?= $imagenFlechaVerde; ?>';
    var flecha_amarilla = '<?= $imagenFlechaAmarilla; ?>';
    var flecha_roja = '<?= $imagenFlechaRoja; ?>';
    var flecha_stop = '<?= $imagenFlechaStop; ?>';
    var flecha_stop2 = '<?= $imagenFlechaStop2; ?>';
    var flecha_stop3 = '<?= $imagenFlechaStop3; ?>';

    function getVehiculosGrupos() {

        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando información .....</span>');
        $("#mensajes").show("slow");
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/rest/get-grupo-vehiculos'; ?>",
            dataType: "html",
            async: true,
            success: function (response) {
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
                if (response != "" && response != undefined) {
                    var responseJson = JSON.parse(response);
                    if (responseJson.estado == 'OK') {
                        gruposVehiculosArrayRest2 = responseJson.mensaje;
                        console.log('------- VEHICULOS GRUPOS ------');
                        console.log(gruposVehiculosArrayRest2);
                        dibujaGrupos();
                        getDataSearch();
                    } else {
                        alert("Problemas recibiendo datos: ERROR en Respuesta");
                    }
                } else {
                    alert("El servicio no retorno datos, refresque la pagina para reintentar.");
                    getVehiculosGrupos();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
        return true;
    }

    function dibujaGrupos() {
        var grupos = '<div class="panel-group" id="listado_grupos" role="tablist" aria-multiselectable="true">';
        var contador = 0;
        var totalVehiculos = 0;
        markersArray = [];
        $.each(gruposVehiculosArrayRest2, function (index) {
            var key = this.id;
            var item = this.nombre;
            var grupoName = this.nombre;
            var patentes = '';
            var patentesArray = this.vehiculo;
            var cantidadPatentes = patentesArray.length;
            totalVehiculos = totalVehiculos + cantidadPatentes;
            $.each(patentesArray, function (index) {
                var patente = this;
                var key = this.vhid;
                var flechaColor = getFlechaMovil(patente);
                vehiculosArrayRest2[patente.vhid] = patente;
                var patenteOption = '<span class="patenteMuestraClass patentepatente" style="display:block">' + patente.patente + '</span>';
                var muetraOption = '<span class="patenteMuestraClass patentemuestra" style="display:none">' + patente.muestra + '</span>';
                var itemName = patenteOption + muetraOption;
                var patenteMuestra = '<div class="itemlista_nombre">' +
                        '<span id="nombre_' + key + '" onclick="muestraVehiculo(' + key + ');" title="' + item + '" class="btn btn-default btn-xs nombreclase" style="width:100%;text-align:left;border:none;margin:3px;color:blue">' + itemName + '</span>' +
                        '<a name="p_' + key + '"></a>' +
                        '</div>';
                var fechaValor = formatDateRest2(patente.fecha);
                var comandos = '';
                var refrescar = '<span id="forzar_' + key + '" onclick="playback(' + key + ');" title="playback" style="margin-left:15px;cursor:pointer"><i style="color:green" class="fa fa-play"></i></span>';
                var flecha = '<span data-patente="' + key + '" class="patente_icon" style="margin-left:5px;cursor:pointer">' + flechaColor + '</span>';
                var fecha = '<span data-patente="' + key + '" class="patente_fecha">' + fechaValor + '</span>';
                var botones = '<div class="itemlista_icon">' + flecha + comandos + refrescar + '</div>';
                var fechaDiv = '<div class="itemlista_fecha">' + fecha + '</div>';
                var clear = '<div style="float:none">&nbsp;</div>';
                var lineaVehiculo = '<div style="height:30px;font-size:12px;margin-bottom:5px;border-bottom:0.1em solid lightgray">' + patenteMuestra + fechaDiv + botones + '</div>';
                patentes = patentes + lineaVehiculo;
                // creating market
                createMarketVehiculo(patente);
            });
            // patentes = patentes + '</div>';
            var linea = '<div class="panel panel-default">' +
                    '<div class="panel-heading" role="tab" id="headingOne">' +
                    '<h5 class="panel-title">' +
                    '<a role="button" data-toggle="collapse" data-parent="#listado_grupos" href="#listado_grupos-collapse' + contador + '" aria-controls="#listado_grupos-collapse' + contador + '" aria-expanded="false" style="font-size:12px;">' +
                    '<i class="fa fa-users"></i> ' + grupoName + ' ( ' + cantidadPatentes + ' )' +
                    '</a>' +
                    '</h5>' +
                    '</div>' +
                    '<div id="listado_grupos-collapse' + contador + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">' +
                    '<div class="panel-body">' + patentes + '</div>' +
                    '</div>' +
                    '</div>';
            grupos = grupos + linea;
            contador++;
        });
        grupos = grupos + '</div>';
        $("#cantidad_vehiculos").html(totalVehiculos);
        $("#listados").html(grupos);
        $('#listado_grupos .panel-collapse').collapse('show');
        centrarVistaArrMarkersTodos();
    }



    function createMarketVehiculo(vehiculo) {
        var iconoMovil = getIconoMovil(vehiculo);
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(vehiculo.latitud), lng: parseFloat(vehiculo.longitud)},
            map: map,
            icon: iconoMovil
        });
        marker.vehiculo = vehiculo;
        markersArray[vehiculo.vhid] = marker;
        playbackMarkersLista2.push = marker.position;
        listadoMarkers.push(marker);
        var infowindow = new google.maps.InfoWindow;
        marker.addListener('click', function (event) {
            if (infowindow) {
                infowindow.close();
            }
            getDireccion(vehiculo, infowindow, marker, event);
        });
        google.maps.event.addListener(map, "click", function (event) {
            infowindow.close();
        });
    }

    function muestraVehiculo(id) {
        apagaPlayback();
        var marker = markersArray[id];
        if (marker != undefined) {
            if (marker.position != undefined) {
                map.panTo(marker.position);
                map.setZoom(16);
                new google.maps.event.trigger(marker, 'click');
            }
        } else {
            return false;
        }
    }

    function getDataSearch() {
        var data = [];
        if (gruposVehiculosArrayRest2 != undefined && gruposVehiculosArrayRest2.length > 0) {
            $(gruposVehiculosArrayRest2).each(function (index) {
                var vehiculoArray = this.vehiculo;
                $.each(vehiculoArray, function (index) {
                    vehiculosArray[this.vhid] = this;
                    var patente = this.muestra + ' (' + this.patente + ' )';
                    var id = this.vhid;
                    data.push({id: id, text: patente});
                });
            });
        }

        $("#patente").select2({
            data: data,
            dropdownCssClass: "listado_patentes",
            allowClear: false,
            theme: "krajee",
            width: "100%",
            placeholder: "Seleccione Patente..",
            language: "es",
            themeCss: ".select2-container--krajee",
            sizeCss: "",
            doReset: true,
            doToggle: false,
            doOrder: false
        });
        $('#patente').on('change', function () {
            buscaPatente(this.value);
        });
    }

    function buscaPatente(id) {
        apagaPlayback();
        muestraVehiculo(id);
        $('#listado_grupos .panel-collapse').collapse('show');
        $(".nombreclase").css("background-color", "white");
        $("#nombre_" + id).css("background-color", "yellow");
        var aTag = $("a[name='p_" + id + "']");
        if (aTag.offset() != undefined) {
            $('#listados').animate({scrollTop: aTag.offset().top}, 'slow');
        }
    }


    var patenteplayback = 0;
    var playbackMarkersArray = [];
    var playbackMarkersList = [];
    var playbackLineasList = [];

    function playback(vhid) {
        apagaPlayback();
        var dataVehiculo = vehiculosArray[vhid];
        patenteplayback = vhid;
        var patente = dataVehiculo.muestra;
        $("#tituloplayback").html(patente);
        $("#playbackdiv").show("slow");
    }

    function buscaPlayback() {
        var url = '';
        var dataVehiculo = vehiculosArray[patenteplayback];
        var avserie = dataVehiculo.av_serie;
        var fecha_desde = $("#fecha_desde").val();
        var hora_desde = $("#hora_desde").val();
        var fecha_hasta = $("#fecha_hasta").val();
        var hora_hasta = $("#hora_hasta").val();
        if (fecha_desde=="" || fecha_hasta==""){
            alert("Seleccione un rango de fechas antes de continuar con el PlayBack.");
            return false;
        }
        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando información .....</span>');
        $("#mensajes").show("slow");
        playbackMarkersArray = [];
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/rest/playback'; ?>",
            dataType: "html",
            async: true,
            data: {avserie: avserie, fecha_desde: fecha_desde, hora_desde: hora_desde, fecha_hasta: fecha_hasta, hora_hasta: hora_hasta},
            success: function (response) {
                $("#mensajes").html('');
                $("#mensajes").show("slow");
                var responseJson = JSON.parse(response);
                if (responseJson.estado == 'OK') {
                    playbackMarkersArray = responseJson.mensaje;
                    console.log('------- VEHICULOS PLAYBACk ------');
                    console.log(playbackMarkersArray);
                    dibujaPlayBack();
                    centrarVistaArrMarkers()
                } else {
                    alert("Problemas recibiendo datos: ERROR en Respuesta");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
        return url;
    }

    function dibujaMarkerPlayback(vehiculo) {
        var iconoMovil = getIconoMovilPlayback(vehiculo);
        pointLabel = formatDateSky(vehiculo.evfecha);
        if (isNaN(parseFloat(vehiculo.latitud)) || isNaN(parseFloat(vehiculo.longitud)) || vehiculo.latitud == undefined || vehiculo.longitud == undefined || vehiculo.latitud == null || vehiculo.longitud == null) {
            return false;
        }
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(vehiculo.latitud), lng: parseFloat(vehiculo.longitud)},
            map: map,
            title: pointLabel,
            icon: iconoMovil
        });
        marker.vehiculo = vehiculo;
        // playbackMarkersList[vehiculo.vhid] = marker;
        playbackMarkersList.push(marker);
        var infowindow = new google.maps.InfoWindow;
        marker.addListener('click', function (event) {
            if (infowindow) {
                infowindow.close();
            }
            getDireccionPlayback(vehiculo, infowindow, marker, event);
        });
        google.maps.event.addListener(map, "click", function (event) {
            infowindow.close();
        });
        return marker;
    }

    function ocultaOtrosMarcadores() {
        $(listadoMarkers).each(function (index) {
            // console.log(this);
            this.setVisible(false);
        });
    }

    function muestraOtrosMarcadores() {
        $(listadoMarkers).each(function (index) {
            // console.log(this);
            this.setVisible(true);
        });
    }

    function limpiaPlayback() {
        $.each(playbackMarkersList, function () {
            this.setMap(null);
        });

        $.each(playbackLineasList, function () {
            this.setMap(null);
        });
        playbackLineasList = [];
        playbackMarkersList = [];
    }

    function apagaPlayback() {
        limpiaPlayback();
        muestraOtrosMarcadores();
        getVehiculosGrupos();
        $("#playbackdiv").hide("slow");
    }

    var playbackMarkersLista = [];
    var playbackMarkersLista2 = [];
    var listadoMarketPlayback = [];

    function dibujaPlayBack() {
        limpiaPlayback();
        ocultaOtrosMarcadores();
        var puntoprevio = null;
        var contador = 0;
        $(playbackMarkersArray).each(function (index) {
            if (this.latitud != "" && this.longitud && isNaN(parseFloat(this.latitud)) == false && isNaN(parseFloat(this.longitud)) == false && this.latitud != undefined && this.longitud != undefined && this.latitud != null && this.longitud != null)
            {
                var currentMarker = dibujaMarkerPlayback(this);
                if (puntoprevio != null && contador < playbackMarkersArray.length) {
                    // linea roja 
                    var lineCoordinates = [puntoprevio.position, currentMarker.position];
                    var line = new google.maps.Polyline({
                        path: lineCoordinates,
                        strokeColor: "#FF0000",
                        strokeOpacity: 1,
                        strokeWeight: 2,
                        geodesic: true,
                        map: map
                    });
                    line.setMap(map);
                    playbackLineasList.push(line);
                    playbackMarkersLista.push(puntoprevio.position);
                    playbackMarkersLista.push(currentMarker.position);
                }
                puntoprevio = currentMarker;
                contador++;
            }
        });
    }

    function enviarComando(vhid, comando) {
        var dataVehiculo = vehiculosArray[vhid];
        var avserie = dataVehiculo.av_serie;
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/rest/envia-comando'; ?>",
            dataType: "html",
            async: true,
            data: {avserie: avserie, vhid: vhid, comando: comando},
            success: function (response) {
                var responseJson = JSON.parse(response);
                alert(response);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
        return url;
    }
    
    function validaFechas()
    {
        var fechadesde = $("#fecha_desde").val();
        var fechahasta = $("#fecha_hasta").val();
        if (fechadesde == "" || fechahasta == "") {
            return false;
        }
        var desde = fechadesde.split("/");
        var hasta = fechahasta.split("/");
        var fecha1 = new Date(parseInt(desde[2], 10), parseInt(desde[1], 10) - 1, desde[0]);
        var fecha2 = new Date(parseInt(hasta[2], 10), parseInt(hasta[1], 10) - 1, hasta[0]);
        if (fecha2 < fecha1)
        {
            alert("La fecha hasta no puede ser menor que la fecha desde.");
            $("#fecha_hasta").val("");
            return false;
        }
    }


    // ***********************************************************


</script>       
<style>
    #patente_div .select2-container {
        font-size: 12px;
        border: 1px solid gray;
    }
    #patente_div .select2-selection__rendered {
        font-size: 12px;
        padding-left: 5px;
        margin-left: -14px;
        margin-top: -8px;
        width:170px;
    }

    .listado_patentes {
        font-size:12px;
    }

    .panel-body {
        padding: 5px;
    }

    .itemlista_nombre{
        text-align:left;
        width:150px;
        max-width: 150px;
        float:left;
    }

    .itemlista_icon{
        text-align:left;
        padding-top:5px;
        /*padding-bottom:5px;*/
        width:80px;
        max-width: 80px;
        height:25px;
        float:left;
        /*border:1px solid red;*/
    }

    .itemlista_fecha{
        text-align:center;
        padding:0px;
        width:40px;
        max-width: 40px;
        height:25px;
        float:left;
        font-size:9px!important;
        white-space: pre-wrap!important;
        /*border:1px solid red;*/
    }

    .panel-heading {
        padding: 7px;
        border-bottom: 1px solid lightgray;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }

    .topbar .topbar-left {
        float: left;
        position: relative;
        width: 330px;
        z-index: 1;
        background-color: #3bafda;
        max-height: 70px;
    }

    .badge {
        font-weight: auto;
        padding: 4px;
        font-size: 14px;
        margin-bottom: 5px;
    }

    #map{
        height: calc(100vh - 100px);
    }

    .content{
        margin-bottom: unset !important;
        padding: unset !important;
    }
    .container-fluid {
        padding-right: unset;
        padding-left: unset;

    }

    .card-box {
        padding: unset;
        border: none;
        border-radius: unset;
        -moz-border-radius: unset;
        margin-bottom: unset;
    }

    .glyphicon-time {
        display:none!important;
    }

    .listado_class {
        padding:5px;
        /*border:1px solid red;*/
    }
    
    

    .frow {
        clear:both;
        padding:1px;
    }

    .frow .col {
        display: block;
        float:left;
        /*margin: 1% 0 1% 1.6%;*/
        /*background:#CCC;*/
        /*padding:2%;*/
    }

    .frow .col:first-child { margin-left: 0; }

    .frow .col4 {
        width: 70%;
    }

    .frow .col3 {
        width: 25.26%;
    }


    @media only screen and (max-width: 480px) {
        .col {  margin: 1% 0 1% 0%;  margin-left: 0 !important;}
        .frow .col3, .frow .col4 { width: 100%; }
    }



</style>