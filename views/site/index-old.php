<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\View;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Collapse;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

$this->title = 'SkyView Vista - Bermann';
$this->params['bread1'] = $this->title;
$this->params['activeLink'] = "inicio";


?>

<?php
  if (isset($_GET['token'])==true && $_GET['token']!=""){
      Yii::$app->session->set('token_apisky',$_GET['token']);
  }
?>

<style>
   #calendar {
  margin: 40px auto;
}
.bac {
    background: #fff;
    padding: 1px 51px;
}
</style>

<!--filter start here--> 
      
<div class="row filter pt-4 pb-5 px-5">

    <div class="col-xl-2 col-lg-2">
        <div class="dashboard_select">
            <label>Flota</label>
            <select class="form-select custom_select" aria-label="Default select example">
            <option selected>CCU RM</option>
            <option value="1">CTYW-75</option>
            <option value="2">GTFC-81</option>
            <option value="3">KFHX-10</option>
            </select>
        </div>
    </div>

    <div class="col-xl-3 col-lg-3">
        <div class="form-group" id="rangestart">
            <label>Start Date</label>
            <!-- <input type="text" placeholder="Date/Time" class="form-control"> -->
            <input type="text"  class="start_date form-control" name="start_date" placeholder="Start Date" autocomplete="off" >

        </div>
    </div>

    <div class="col-xl-3 col-lg-3">
        <div class="form-group" id="rangeend">
            <label>End Date</label>
            <input type="text" class="end_date form-control" name="finish_date" placeholder="End Date" autocomplete="off" >
            <!-- <input type="text" placeholder="Date/Time" class="form-control"> -->
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 d-flex align-items-end justify-content-lg-end justify-content-sm-center">
        <div class="w-100">
            <button type="button" class="btn me-1 button_big">Vehicles Label-Licenceplate <img src="<?= Yii::getAlias('@web'); ?>/assets/img/search_white.png" class="ms-2"></button>
            <button type="button" class="btn"><img src="<?= Yii::getAlias('@web'); ?>/assets/img/filter-icon.png"></button>
        </div>
    </div>

</div>

<!--filter end here-->
<div class="bac">
    <section class="track_speed_tack mb-5">
        <!-- <div class="container">
            <img src="<?= Yii::getAlias('@web'); ?>/assets/img/chart_img.jpg">
        </div> -->
        <!-- <div class="cal-wrapper"><div id='calendar'></div></div> -->
        <div id='calendar'></div>
    </section>
</div>

<script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/datetime/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/datetime/jquery-ui-sliderAccess.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- calenderjs -->
<!-- <script type="text/javascript" src="https://unpkg.com/fullcalendar@5.3.2/main.min.js"></script> -->

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/core.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/interaction.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/timeline.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/resource-common.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/resource-timeline.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/moment.js"></script>


<script type="text/javascript">
	function getParameterByName(name, url) {    
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	    results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	var getParameter = getParameterByName('date');
	if(getParameter){
	    var filtermindate = getParameter;
	} else{
	    var filtermindate = '';
	} 
	
	$(".start_date").datetimepicker({
	    dateFormat: 'yy-mm-dd',
	    disabled: false,
	    // minDate:new Date(),
	});

	$('.end_date').datetimepicker({
	    dateFormat: 'yy-mm-dd',
	    disabled: false,

	});
</script>    


<script>
var data = { "estado": "OK", "mensaje":         { "in_out": [ { "patente": "KFHZ-77", "nombre_poligono": "Oficina Bermann FULLSGD", "color_poligono": "#fc2626", "umbral_sobre_estadia": "00:00:00", "fecha_entrada": "2022-01-05 03:00:00", "hito_umbral": null, "fecha_salida": "2022-01-05 00:00:00", "poligono_geom": "0103000020E6100000010000001100000099A5DB2171B940C0000004BF81A951C0343B5BBB56B940C00000048543A951C00321E0BE72B940C00000842541A951C0CD3A63D396B940C0000004123EA951C0F53CB128A9B940C00000048A7AA951C024EF14B887B940C0000084D87EA951C05A008D6976B940C00000849A80A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C0" },{ "patente": "KFHZ-77", "nombre_poligono": "lazo", "color_poligono": "#000000", "umbral_sobre_estadia": "00:00:00", "fecha_entrada": "2022-01-05 05:00:00", "hito_umbral": null, "fecha_salida": "2022-01-05 03:00:00", "poligono_geom": "0103000020E610000001000000070000006377A88D4EC040C00000006CA9AB51C06AD4508443BE40C0000000801DAB51C04A42841D66BE40C00000001CFBA951C07F1F8B1377BF40C0000000702EAA51C0F749E17C62C040C00000000C47AA51C0C9A9544A9EC040C0000000308BAB51C06377A88D4EC040C00000006CA9AB51C0" },
{"title": 'Birthday Party',
      "start": '2022-01-05T07:00:00',
      "backgroundColor": 'green',
      "borderColor": 'green'
    } ] }, "t_ejecucion": 0.069000000000000006 };
var eventsArray = data.mensaje.in_out.map(function(v) { 
    return {
        resourceId: v.patente,
        start: moment(v.fecha_salida).format(),
        end: moment(v.fecha_entrada).format(),
        // eventColor: v.color_poligono
    };
});
// var resourcesArray = data.mensaje.detenciones.map(function(v) { 
var resourcesArray = data.mensaje.in_out.map(function(v) { 
    return {
        id: v.patente,
        title: v.patente,
        eventColor: v.color_poligono
    };
})
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'interaction', 'resourceTimeline', 'listPlugin' ],
        // timeZone: 'UTC',
        timeZone: 'UTC+05:30',
        defaultView: 'resourceTimelineDay',
        aspectRatio: 2.0,
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'resourceTimelineDay,resourceTimelineWeek,resourceTimelineMonth'
        },
        resourceLabelText: 'Vehicles Label Licenceplate',
        resources: resourcesArray,
        events: eventsArray
    });
    calendar.render();
});
    

</script>





