<?php

// use yii;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\View;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Collapse;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

$this->title = 'SkyView Vista - Bermann';
$this->params['bread1'] = $this->title;
$this->params['activeLink'] = "inicio";

// print_r($result); 

?>

<?php
  if (isset($_GET['token'])==true && $_GET['token']!=""){
      Yii::$app->session->set('token_apisky',$_GET['token']);
  }

?>
<?php
// echo $this->result; 
// print_r($result);
// var_dump($result);

?>
<style>
   /* #calendar {
  margin: 40px auto;
}
.bac {
    background: #fff;
    padding: 1px 51px;
} */

/* html, body {
  margin: 0;
  padding: 0;
  font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
  font-size: 14px;
} */

#calendar {
  max-width: 100%;
  margin: 40px auto;
}
#calendar .fc-ltr .fc-timeline-event {
  margin-right: 1px;
  border-radius: 10px !important;
}

.fc-unthemed .fc-content, .fc-unthemed .fc-divider, .fc-unthemed .fc-list-heading td, .fc-unthemed .fc-list-view, .fc-unthemed .fc-popover, .fc-unthemed .fc-row, .fc-unthemed tbody, .fc-unthemed td, .fc-unthemed th, .fc-unthemed thead {
  border-color: #ddd;
  border: none !important;
}

.fc-time-area .fc-content table tr td {
  border: 4px solid #fff;
  position: relative;
}
.fc-time-area .fc-content table tr td:after {
  position: absolute;
  height: 1px;
  width: 100%;
  background: #c0c0c0;
  content: "";
  top: 8px;
}

#calendar td.fc-resource-area.fc-widget-header {
  width: 10% !important;
}
.fc-ltr .fc-timeline-event {
  margin-right: 1px;
  /* border-radius: 5px !important; */
}

.fc-event {
    margin: -1px -1px;
}






.tooltip
{
opacity: 1;
}
</style>

<!--filter start here--> 
      
<div class="row filter pt-4 pb-5 px-5">

    <div class="col-xl-2 col-lg-2">
        <div class="dashboard_select">
            <label>Flota</label>
            <select class="form-select custom_select" aria-label="Default select example">
            <option selected>CCU RM</option>
            <option value="1">CTYW-75</option>
            <option value="2">GTFC-81</option>
            <option value="3">KFHX-10</option>
            </select>
        </div>
    </div>

    <div class="col-xl-3 col-lg-3">
        <div class="form-group" id="rangestart">
            <label>Start Date</label>
            <!-- <input type="text" placeholder="Date/Time" class="form-control"> -->
            <input type="text"  class="start_date form-control" name="start_date" placeholder="Start Date" autocomplete="off" >

        </div>
    </div>

    <div class="col-xl-3 col-lg-3">
        <div class="form-group" id="rangeend">
            <label>End Date</label>
            <input type="text" class="end_date form-control" name="finish_date" placeholder="End Date" autocomplete="off" >
            <!-- <input type="text" placeholder="Date/Time" class="form-control"> -->
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 d-flex align-items-end justify-content-lg-end justify-content-sm-center">
        <div class="w-100">
            <button type="button" class="btn me-1 button_big">Vehicles Label-Licenceplate <img src="<?= Yii::getAlias('@web'); ?>/assets/img/search_white.png" class="ms-2"></button>
            <button type="button" class="btn"><img src="<?= Yii::getAlias('@web'); ?>/assets/img/filter-icon.png"></button>
        </div>
    </div>

</div>

<!--filter end here-->
<div class="bac">
    <section class="track_speed_tack mb-5">
        <!-- <div class="container">
            <img src="<?= Yii::getAlias('@web'); ?>/assets/img/chart_img.jpg">
        </div> -->
        <!-- <div class="cal-wrapper"><div id='calendar'></div></div> -->
        <div id='calendar'></div>
    </section>
</div>

<script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/datetime/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/datetime/jquery-ui-sliderAccess.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- calenderjs -->
<!-- <script type="text/javascript" src="https://unpkg.com/fullcalendar@5.3.2/main.min.js"></script> -->

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/core.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/interaction.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/timeline.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/resource-common.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/resource-timeline.main.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/calender/js/moment.js"></script>
<script src='https://unpkg.com/popper.js/dist/umd/popper.min.js'></script>
<script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>
<script type="text/javascript">
	function getParameterByName(name, url) {    
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	    results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	var getParameter = getParameterByName('date');
	if(getParameter){
	    var filtermindate = getParameter;
	} else{
	    var filtermindate = '';
	} 
	
	$(".start_date").datetimepicker({
	    dateFormat: 'yy-mm-dd',
	    disabled: false,
	    // minDate:new Date(),
	});

	$('.end_date').datetimepicker({
	    dateFormat: 'yy-mm-dd',
	    disabled: false,

	});
</script>    


<script>
var data = <?php echo $result; ?>   
// var data = { "estado": "OK", "mensaje": { "in_out": [ 
//     {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "lazo",
//                 "color_poligono": "#04ff00",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 00:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 01:55:48",
//                 "poligono_geom":"0103000020E610000001000000070000006377A88D4EC040C00000006CA9AB51C06AD4508443BE40C0000000801DAB51C04A42841D66BE40C00000001CFBA951C07F1F8B1377BF40C0000000702EAA51C0F749E17C62C040C00000000C47AA51C0C9A9544A9EC040C0000000308BAB51C06377A88D4EC040C00000006CA9AB51C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "Oficina Bermann FULLSGD",
//                 "color_poligono": "#000000",
//                 "umbral_sobre_estadia": "00:10:00",
//                 "fecha_entrada": "2022-01-06 01:55:49",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 02:05:49",
//                 "poligono_geom":"0103000020E6100000010000001100000099A5DB2171B940C0000004BF81A951C0343B5BBB56B940C00000048543A951C00321E0BE72B940C00000842541A951C0CD3A63D396B940C0000004123EA951C0F53CB128A9B940C00000048A7AA951C024EF14B887B940C0000084D87EA951C05A008D6976B940C00000849A80A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#04ff00",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 02:05:50",
//                 "hito_umbral": "2022-01-06 09:36:23",
//                 "fecha_salida": "2022-01-06 02:55:50",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#fc2626",
//                 "umbral_sobre_estadia": "00:50:00",
//                 "fecha_entrada": "2022-01-06 02:55:50",
//                 "hito_umbral": "2022-01-06 09:36:23",
//                 "fecha_salida": "2022-01-06 03:55:50",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "Santa Isabel",
//                 "color_poligono": "#04ff00",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 05:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 10:00:00",
//                 "poligono_geom":"0103000020E6100000010000000C0000002EAACDADACB940C0FCFF3F6C5CA951C0279B8C2F94B940C0FCFFFFE45FA951C0445AA05D98B940C0FCFF5FAE72A951C0A278017EB0B940C0FCFFBFC771A951C0BBCAA6D6C8B940C0000040EF6FA951C001692CDAADB940C0FCFF3FB324A951C04A6330A094B940C0FCFF3FA5F6A851C09AC2D22166B940C0FCFF3FF19BA851C0A8186C8651B940C0FCFF3F0AA1A851C088B63DB17BB940C0FCFF3F59F7A851C0A0D11E8F97B940C0FCFF3FDD27A951C02EAACDADACB940C0FCFF3F6C5CA951C0"
//             },

//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "Casa Nicolas T\u00e9cnico",
//                 "color_poligono": "#000000",
//                 "umbral_sobre_estadia": "00:10:00",
//                 "fecha_entrada": "2022-01-06 10:00:00",
//                 "hito_umbral": "2022-01-06 08:00:00",
//                 "fecha_salida": "2022-01-06 10:10:00",
//                 "poligono_geom":"0103000020E61000000100000006000000EAFAA7F189C840C000410068DDB251C09232CBAF96C840C000410052D5B251C0BA93C9669FC840C000418092D8B251C02065B8DFA9C840C0004100E1DCB251C07EE5B3CF99C840C000000063E7B251C0EAFAA7F189C840C000410068DDB251C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "Senavin",
//                 "color_poligono": "#04ff00",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 10:10:00",
//                 "hito_umbral": "2022-01-06 16:28:28",
//                 "fecha_salida": "2022-01-06 12:10:00",
//                 "poligono_geom":"0103000020E61000000100000005000000BDE773CB68C540C000140052B9B051C002874F6D7CC540C00014809CA3B051C0F39CBF4B9FC540C0001400A4B2B051C0E8B27EB08CC540C000148078C7B051C0BDE773CB68C540C000140052B9B051C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "CCTI",
//                 "color_poligono": "#fc2626",
//                 "umbral_sobre_estadia": "01:50:00",
//                 "fecha_entrada": "2022-01-06 12:10:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 14:00:00",
//                 "poligono_geom":"0103000020E610000001000000050000000F706621BFBB40C00058E800DEB051C00F706621BFBB40C00058E894C2B051C0BAB20A7F20BC40C00058E824BAB051C051E21C2534BC40C00058E828D4B051C00F706621BFBB40C00058E800DEB051C0"
//             },


//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "Casa Nicolas T\u00e9cnico",
//                 "color_poligono": "#3004b4",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 16:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 21:00:00",
//                 "poligono_geom":"0103000020E61000000100000006000000EAFAA7F189C840C000410068DDB251C09232CBAF96C840C000410052D5B251C0BA93C9669FC840C000418092D8B251C02065B8DFA9C840C0004100E1DCB251C07EE5B3CF99C840C000000063E7B251C0EAFAA7F189C840C000410068DDB251C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "CCTI",
//                 "color_poligono": "#fc2626",
//                 "umbral_sobre_estadia": "01:00:00",
//                 "fecha_entrada": "2022-01-06 21:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 22:00:00",
//                 "poligono_geom":"0103000020E610000001000000050000000F706621BFBB40C00058E800DEB051C00F706621BFBB40C00058E894C2B051C0BAB20A7F20BC40C00058E824BAB051C051E21C2534BC40C00058E828D4B051C00F706621BFBB40C00058E800DEB051C0"
//             },
//             {
//                 "patente": "KFHZ-77",
//                 "nombre_poligono": "Casa Nicolas T\u00e9cnico",
//                 "color_poligono": "#3004b4",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 23:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 23:59:59",
//                 "poligono_geom":"0103000020E61000000100000006000000EAFAA7F189C840C000410068DDB251C09232CBAF96C840C000410052D5B251C0BA93C9669FC840C000418092D8B251C02065B8DFA9C840C0004100E1DCB251C07EE5B3CF99C840C000000063E7B251C0EAFAA7F189C840C000410068DDB251C0"
//             },



//             //Next Vehicle
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "Oficina Bermann FULLSGD", 
//                 "color_poligono": "#04ff00", 
//                 "umbral_sobre_estadia": "00:00:00", 
//                 "fecha_entrada": "2022-01-06 00:00:00", 
//                 "hito_umbral": null, 
//                 "fecha_salida": "2022-01-06 02:00:00","poligono_geom":"0103000020E6100000010000001100000099A5DB2171B940C0000004BF81A951C0343B5BBB56B940C00000048543A951C00321E0BE72B940C00000842541A951C0CD3A63D396B940C0000004123EA951C0F53CB128A9B940C00000048A7AA951C024EF14B887B940C0000084D87EA951C05A008D6976B940C00000849A80A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "Oficina Bermann FULLSGD",
//                 "color_poligono": "#000000",
//                 "umbral_sobre_estadia": "00:10:00",
//                 "fecha_entrada": "2022-01-06 02:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 02:10:00",
//                 "poligono_geom":"0103000020E6100000010000001100000099A5DB2171B940C0000004BF81A951C0343B5BBB56B940C00000048543A951C00321E0BE72B940C00000842541A951C0CD3A63D396B940C0000004123EA951C0F53CB128A9B940C00000048A7AA951C024EF14B887B940C0000084D87EA951C05A008D6976B940C00000849A80A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "Oficina Bermann FULLSGD", 
//                 "color_poligono": "#04ff00", 
//                 "umbral_sobre_estadia": "00:00:00", 
//                 "fecha_entrada": "2022-01-06 02:10:00", 
//                 "hito_umbral": null, 
//                 "fecha_salida": "2022-01-06 05:10:00","poligono_geom":"0103000020E6100000010000001100000099A5DB2171B940C0000004BF81A951C0343B5BBB56B940C00000048543A951C00321E0BE72B940C00000842541A951C0CD3A63D396B940C0000004123EA951C0F53CB128A9B940C00000048A7AA951C024EF14B887B940C0000084D87EA951C05A008D6976B940C00000849A80A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#fc2626",
//                 "umbral_sobre_estadia": "00:50:00",
//                 "fecha_entrada": "2022-01-06 05:10:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 06:00:00",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#04ff00", 
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 06:30:00",
//                 "hito_umbral": "2021-11-15 16:20:49",
//                 "fecha_salida": "2022-01-06 08:00:00",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#3004b4",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 09:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 14:00:00",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "Oficina Bermann FULLSGD",
//                 "color_poligono": "#000000",
//                 "umbral_sobre_estadia": "00:10:00",
//                 "fecha_entrada": "2022-01-06 14:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 14:10:00",
//                 "poligono_geom":"0103000020E6100000010000001100000099A5DB2171B940C0000004BF81A951C0343B5BBB56B940C00000048543A951C00321E0BE72B940C00000842541A951C0CD3A63D396B940C0000004123EA951C0F53CB128A9B940C00000048A7AA951C024EF14B887B940C0000084D87EA951C05A008D6976B940C00000849A80A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#3004b4",
//                 "umbral_sobre_estadia": "00:00:00",
//                 "fecha_entrada": "2022-01-06 14:10:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 17:10:00",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#fc2626",
//                 "umbral_sobre_estadia": "02:50:00",
//                 "fecha_entrada": "2022-01-06 17:10:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 20:00:00",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             },
            
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "Oficina Bermann FULLSGD", 
//                 "color_poligono": "#04ff00", 
//                 "umbral_sobre_estadia": "00:00:00", 
//                 "fecha_entrada": "2022-01-06 21:00:00", 
//                 "hito_umbral": null, 
//                 "fecha_salida": "2022-01-06 23:00:00","poligono_geom":"0103000020E6100000010000001100000099A5DB2171B940C0000004BF81A951C0343B5BBB56B940C00000048543A951C00321E0BE72B940C00000842541A951C0CD3A63D396B940C0000004123EA951C0F53CB128A9B940C00000048A7AA951C024EF14B887B940C0000084D87EA951C05A008D6976B940C00000849A80A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C099A5DB2171B940C0000004BF81A951C0"
//             },
//             {
//                 "patente": "XY-3478",
//                 "nombre_poligono": "SERVICIO TECNICO",
//                 "color_poligono": "#fc2626",
//                 "umbral_sobre_estadia": "01:00:00",
//                 "fecha_entrada": "2022-01-06 23:00:00",
//                 "hito_umbral": null,
//                 "fecha_salida": "2022-01-06 23:59:59",
//                 "poligono_geom":"0103000020E61000000100000006000000F6707B1673B940C00060006A7AA951C0F21981169FB940C0006000E676A951C073F1F7F093B940C00060009450A951C01C3C643F63B940C0006000CC54A951C020EDEA5371B940C00060006A7AA951C0F6707B1673B940C00060006A7AA951C0"
//             }

            
// ] }, "t_ejecucion": 0.069000000000000006 };
var eventsArray = data.mensaje.in_out.map(function(v) { 
    return {
        resourceId: v.patente,
        start: moment(v.fecha_entrada).format(),
        end: moment(v.fecha_salida).format(),
        color: v.color_poligono,
        // title: v.nombre_poligono,
        stay: v.umbral_sobre_estadia,
        polygon_name: v.nombre_poligono,
        // display: 'background'

    };
});
// var resourcesArray = data.mensaje.detenciones.map(function(v) { 
var resourcesArray = data.mensaje.in_out.map(function(v) { 

    return {
        id: v.patente,
        title: v.patente,
        // eventColor: v.color_poligono
    };
})
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {

        plugins: [ 'interaction', 'resourceTimeline' ],
        // timeZone: 'UTC',
        timeZone: 'UTC+05:30',
        defaultView: 'resourceTimelineDay',
        aspectRatio: 2.5,
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'resourceTimelineDay,resourceTimelineWeek,resourceTimelineMonth'
        },
        eventRender: function(info) {
          var popup = '<div>Polygon Name : ' + info.event.extendedProps.polygon_name + 
          // '<br>Start-Time : '+ info.event.start+'<br>End-Time : '+ info.event.end+
          '<br>Stay-Time : '+info.event.extendedProps.stay+'</div>';  

          var tooltip = new Tooltip(info.el, {
            title: popup,
            placement: 'top',
            trigger: 'hover',
            container: 'body',
            html: true
          });
          console.log(info.event);
        },

        resourceLabelText: 'Vehicles Label Licenceplate',
        resources: resourcesArray,
        events: eventsArray,
    });
    calendar.render();
});





</script>





