<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\View;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Collapse;

$pagina = $this->context->route;
$imagen = "";
$session = Yii::$app->session;
$idUsuario = $session['IdUsuario'];
$nombreUsuario = $session['nombreUsuario'];

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/images/favicon.png" type="image/x-icon"/>

        <?php $this->head() ?>
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/mantenedor.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/fontAwesome/fa/all.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/style.css" rel="stylesheet" type="text/css">

        <link href="<?= Yii::getAlias('@web'); ?>/assets/js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" crossorigin="anonymous"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/css/style.css" rel="stylesheet">
        <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />

        <!-- date-picker -->
        <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" />
	    <link rel="stylesheet" media="all" type="text/css" href="<?= Yii::getAlias('@web'); ?>/datetime/jquery-ui-timepicker-addon.css" />

        <!-- calender css -->
        <!-- <link href="https://unpkg.com/fullcalendar@5.3.2/main.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet"> -->

        <link href="<?= Yii::getAlias('@web'); ?>/calender/css/core.main.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/calender/css/timeline.main.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/calender/css/resource-timeline.main.min.css" rel="stylesheet" type="text/css">


    </head>
    <body>
        <?php $this->beginBody() ?>


        <!-- Begin page -->
        <div id="wrapper" class="forced">

            <!-- Top Bar Start -->
            <header>
                <div class="container d-flex justify-content-between align-items-center">
                    <a href="" class="header_logo"><img src="<?= Yii::getAlias('@web'); ?>/assets/img/white_logo.png"></a>
                    <ul class="d-flex justify-content-between">
                    <li><a href=""><img src="<?= Yii::getAlias('@web'); ?>/assets/img/search.png"></a></li>
                    <li><a href="" class="notify"><img src="<?= Yii::getAlias('@web'); ?>/assets/img/notifications.png"></a></li>
                    <li><a href="" class="mr-0"><img src="<?= Yii::getAlias('@web'); ?>/assets/img/profile.png"></a></li>
                    </ul>
                </div>
            </header>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->

            
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

                <!-- Start content -->
                <section class="filter mb-4">
                    <div class="container-fluid p-0">
                        <?= $content ?>
                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer">
                    <?= date("Y") ?> <span class="hide-phone">- Bermann </span>
                </footer>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- Plugins  -->

        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/detect.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/fastclick.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.slimscroll.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.blockUI.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/waves.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/wow.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/switchery/switchery.min.js"></script>
        <!-- Custom main Js -->
        <!-- <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.app.js"></script> -->

        <!--JS Files -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/js/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- <script src="<?= Yii::getAlias('@web'); ?>/assets/js/main.js"></script> -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
        <!-- <script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script> -->

        <!-- date picker -->
        <!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
       

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
