<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\View;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Collapse;

$pagina = $this->params['activeLink'];
$imagen = "";
$session = Yii::$app->session;
$idUsuario = $session['IdUsuario'];
$nombreUsuario = $session['nombreUsuario'];

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/images/favicon.png" type="image/x-icon"/>

        <?php $this->head() ?>
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/mantenedor.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/fontAwesome/fa/all.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div id="wrapper">
                <!-- Start content -->
                <div class="content" style="">
                    <div class="container-fluid">
                        <?= $content ?>
                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer">
                    <?= date("Y") ?> <span class="hide-phone">- Bermann </span>
                </footer>
       </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
  
