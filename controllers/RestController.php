<?php

namespace app\controllers;

use Yii;
use stdClass;
use yii\web\Response;
use yii\web\Controller;
use app\models\Usuarios;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\GruposDctUsuarios;
use yii\helpers\Html;

class RestController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    //******************** SKYVIEW VISTA ***************************
    
    public function getCurrentUserSkyName(){
        $session = Yii::$app->session;
        $idUsuario = $session['IdUsuario'];
        $usuarios = \app\models\Usuarios::find()->where(['id'=>$idUsuario])->one();
        $res = '';
        if ($usuarios!=false){
            $res = $usuarios->usuario_sky;
        }
        return $res;
    }
    
    public function getCurrentUserSkyPass(){
        $session = Yii::$app->session;
        $idUsuario = $session['IdUsuario'];
        $usuarios = \app\models\Usuarios::find()->where(['id'=>$idUsuario])->one();
        $res = '';
        if ($usuarios!=false){
            $res = $usuarios->clave_sky;
        }
        return $res;
    }

    public function actionTokenSky() {
        $usuario = $this->getCurrentUserSkyName();
        $claveStr = $this->getCurrentUserSkyPass();
        $token = 0;
        $clave = md5($claveStr);
        $urlBase = 'https://rest2.bermanngps.cl/BermannRest/api/login';
        $url = $urlBase . '?user=' . $usuario . '&pass=' . $clave . '&app=9&os=3';
        // echo $url;
        $ch = curl_init();
        ini_set("curl.cainfo", null);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $resultArray = json_decode($result, true);
        if ($resultArray['estado'] == "OK") {
            if ($resultArray['mensaje'][0]['estado'] == "OK") {
                $token = $resultArray['mensaje'][0]['token'];
                Yii::$app->session->set('token_apisky', $token);
            }
        }
        return $token;
    }

    public function actionGetGrupos2() {
        $token = Yii::$app->session->get('token_apisky');
        $urlBase = 'https://rest2.bermanngps.cl/BermannRest/api/grupos';
        $url = $urlBase . '?tk=' . $token;
        // echo $url;
        $ch = curl_init();
        ini_set("curl.cainfo", null);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function actionGetGrupoVehiculos() {
        $token = Yii::$app->session->get('token_apisky');
        $urlBase = 'https://rest2.bermanngps.cl/BermannRest/api/grupovehiculo';
        $url = $urlBase . '?tk=' . $token;
        // echo $url;
        $ch = curl_init();
        ini_set("curl.cainfo", null);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    
    public function actionEnviaComando() {
        $token = Yii::$app->session->get('token_apisky');
        $vhid = isset($_POST['vhid'])==true && $_POST['vhid']!="" ? $_POST['vhid'] : "";
        $avserie = isset($_POST['avserie'])==true && $_POST['avserie']!="" ? $_POST['avserie'] : "";
        $comando = isset($_POST['comando'])==true && $_POST['comando']!="" ? $_POST['comando'] : "";
        $usuario = $this->getCurrentUserSkyName();
        $claveStr = $this->getCurrentUserSkyPass();
        $clave = md5($claveStr);
        $urlBase = 'https://rest2.bermanngps.cl/BermannRest/api/enviacomando';
        $url = $urlBase . '?tk=' . $token.'&comando='.$comando.'&avserie='.$avserie.'&user='.$usuario.'&md5pass='.$clave;
        // echo $url;
        $ch = curl_init();
        ini_set("curl.cainfo", null);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    
    public function fechaSql($fecha) {
        if ($fecha != "") {
            $f = explode('/', $fecha);
            if (count($f) == 3) {
                $mes = strlen($f[1]) == 1 ? '0' . $f[1] : $f[1];
                $fecha = $f[2] . '-' . $mes . '-' . $f[0];
            }
        }
        return $fecha;
    }
    
    public function actionPlayback() {
        $token = Yii::$app->session->get('token_apisky');
        $avserie = isset($_POST['avserie'])==true && $_POST['avserie']!="" ? $_POST['avserie'] : "";
        $fechaDesde = isset($_POST['fecha_desde'])==true && $_POST['fecha_desde']!="" ? $_POST['fecha_desde'] : "";
        $horaDesde = isset($_POST['hora_desde'])==true && $_POST['hora_desde']!="" ? $_POST['hora_desde'] : "";
        $fechaHasta = isset($_POST['fecha_hasta'])==true && $_POST['fecha_hasta']!="" ? $_POST['fecha_hasta'] : "";
        $horaHasta = isset($_POST['hora_hasta'])==true && $_POST['hora_hasta']!="" ? $_POST['hora_hasta'] : "";
        
        $fechaDesde = $fechaDesde!="" ? $this->fechaSql($fechaDesde) : "";
        $fechaHasta = $fechaHasta!="" ? $this->fechaSql($fechaHasta) : "";
        $fecha = $horaDesde!="" ? $fechaDesde.' '.$horaDesde : $fechaDesde;
        $fecha2 = $horaHasta!="" ? $fechaHasta.' '.$horaHasta : $fechaHasta;
        
        $urlBase = 'https://rest2.bermanngps.cl/BermannRest/api/trayectovehiculorangodl';
        $url = $urlBase . '?tk=' . $token.'&avserie='.$avserie.'&fecha='.urlencode($fecha).'&fecha2='.urlencode($fecha2);
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    // **************************************************************
}
